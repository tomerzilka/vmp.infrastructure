﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vmp.Infrastructure.Core.DataElements.Interfaces
{
    public interface ISchedulerTask
    {
        Task Execute(IContext context);
    }
}
