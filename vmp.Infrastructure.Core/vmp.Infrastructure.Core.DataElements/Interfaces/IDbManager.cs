﻿using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace vmp.Infrastructure.Core.DataElements.Interfaces
{
    public interface IDbManager
    {
        Task<bool> ExecuteGetValue(string sp_name, DbParameter[] parameters, Func<object, Task> dataHandler);
        Task<bool> ExecuteGetValue(string sp_name, DbParameter[] parameters, Action<object> dataHandler);
        Task<bool> ExecuteGetValue<T>(string sp_name, DbParameter[] parameters, Func<T, Task> dataHandler);

        Task<bool> GetDataSet(string sp_name, DbParameter[] parameters, Func<IDataReader, Task> rowHandler);
        Task<bool> GetDataSet(string sp_name, DbParameter[] parameters, Action<IDataReader> rowHandler);

        Task<bool> ExecuteScalar(string command, DbParameter[] parameters);
    }
}
