﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace vmp.Infrastructure.Core.DataElements.Interfaces
{
    public interface IIocManager
    {
        /// <summary>
        /// Get a single static instance of the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>Static instance of T</returns>
        T FirstOrDefault<T>();

        /// <summary>
        /// Get a single static instance of the specified type filtered by expression(T)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>Static instance of T</returns>
        T FirstOrDefault<T>(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// Get a single static instance of TType from grouped by TGroup 
        /// </summary>
        /// <typeparam name="TType"></typeparam>
        /// <returns>Static instance of TType</returns>
        TType FirstOrDefault<TType, TGroup>(Expression<Func<TType, bool>> predicate);

        /// <summary>
        /// Get all static instances of T
        /// </summary>
        /// <typeparam name="T[]"></typeparam>
        /// <returns>Static instances of T</returns>
        IEnumerable<T> Many<T>();

        /// <summary>
        /// Get a new instance of the specified type filtered by expression(T)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>instance of T</returns>
        T Instansiate<T>(Expression<Func<T, bool>> predicate) where T : Instansiatable<T>;
    }
}
