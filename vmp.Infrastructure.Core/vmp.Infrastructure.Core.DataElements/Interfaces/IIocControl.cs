﻿namespace vmp.Infrastructure.Core.DataElements.Interfaces
{
    /// <summary>
    /// Decorate a class with [Export(typeof(IIocControl))] to have it visible to IocManager
    /// Implement the class with IIocControl to find it through it's ExportedName
    /// This interface can be inherited
    /// </summary>
    public interface IIocControl
    {
        string ExportedName { get; }
    }
}
