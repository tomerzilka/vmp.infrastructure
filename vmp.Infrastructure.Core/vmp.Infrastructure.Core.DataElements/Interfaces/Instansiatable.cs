﻿namespace vmp.Infrastructure.Core.DataElements.Interfaces
{
    public interface Instansiatable<T>: IIocControl
    {
        T CreateInstance();
    }
}
