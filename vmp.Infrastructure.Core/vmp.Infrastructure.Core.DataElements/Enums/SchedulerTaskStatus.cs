﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vmp.Infrastructure.Core.DataElements.Enums
{
    public enum SchedulerTaskStatus
    {
        Pending = 1,
        InProgress = 2,
        Finished = 3,
        Failed = 4
    }
}
