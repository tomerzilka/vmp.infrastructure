﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vmp.Infrastructure.Core.DataElements.Enums
{
    public enum TimeDefinition
    {
        Millisecond = 1,
        Second = 2,
        Minute = 3,
        Hour = 4,
        Day = 5,
        Week = 6,
        Month = 7,
        Year = 8,
        Once =9
    }
}
