﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vmp.Infrastructure.Core.DataElements.Interfaces;

namespace vmp.Infrastructure.Core.DataElements.Contexts
{
    public abstract class BaseContext : IContext
    {
        public ILog Logger { get; set; }
        public IDbManager DBManager { get; set; }
    }
}
