﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using vmp.Infrastructure.Core.DataElements.Interfaces;

namespace vmp.Infrastructure.Core.DataElements.Abstracts
{
    public abstract class BaseDbManager<TDbConnection, TDbCommand, TDbParameter> : IDbManager
        where TDbConnection : DbConnection,new()
        where TDbCommand : DbCommand,new()
        where TDbParameter : DbParameter
    {

        private readonly string _connectionString;

        /// <summary>
        /// Uses default connection string "Default" from Configuration file.
        /// </summary>
        public BaseDbManager()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
        }
        public BaseDbManager(string connectionString)
        {
            _connectionString = connectionString;
        }


        protected virtual TDbConnection m_connection
        {
            get
            {
                var connection = new TDbConnection();
                connection.ConnectionString = _connectionString;

                return connection;
            }
        }
        protected virtual TDbCommand CreateCommand(string command, TDbConnection connection)
        {
            TDbCommand cmd = new TDbCommand
            {
                CommandText = command,
                Connection = connection
            };

            return cmd;
        }

        protected abstract TDbParameter CreateParameter(DbParameter value);
        protected virtual TDbParameter[] CreateParameters(DbParameter[] values)
        {
            TDbParameter[] retArr = values.Select(x => CreateParameter(x)).ToArray();
            return retArr;
        }


        public virtual async Task<bool> ExecuteGetValue(string command, DbParameter[] parameters, Func<object, Task> dataHandler)
        {
            bool success = await GetDataSet(command, parameters, async (record) =>
            {
                await dataHandler(record.GetValue(0));
            });

            return success;
        }
        public virtual async Task<bool> ExecuteGetValue(string command, DbParameter[] parameters, Action<object> dataHandler)
        {
            bool success = await GetDataSet(command, parameters, record =>
            {
                dataHandler(record.GetValue(0));
            });

            return success;
        }
        public virtual async Task<bool> ExecuteGetValue<T>(string command, DbParameter[] parameters, Func<T, Task> dataHandler)
        {
            bool success = await GetDataSet(command, parameters, async (record) =>
            {
                var val = record.GetValue(0);
                T convertedVal = (T)Convert.ChangeType(val, typeof(T));
                await dataHandler(convertedVal);
            });

            return success;
        }

        public virtual async Task<bool> GetDataSet(string command, DbParameter[] parameters, Func<IDataReader, Task> rowHandler)
        {
            var connection = m_connection;
            try
            {
                using (TDbCommand cmd = CreateCommand(command, connection))
                {

                    cmd.CommandType = CommandType.Text;
                    if (parameters != null && parameters.Length > 0)
                    {
                        cmd.Parameters.AddRange(CreateParameters(parameters));
                    }

                    await m_connection.OpenAsync();

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            await rowHandler(reader);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }
        public virtual async Task<bool> GetDataSet(string command, DbParameter[] parameters, Action<IDataReader> rowHandler)
        {
            var connection = m_connection;

            try
            {
                using (TDbCommand cmd = CreateCommand(command, connection))
                {
                    cmd.CommandType = CommandType.Text;
                    if (parameters != null && parameters.Length > 0)
                    {
                        cmd.Parameters.AddRange(CreateParameters(parameters));
                    }

                    await connection.OpenAsync();

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            rowHandler(reader);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }
        public virtual async Task<bool> ExecuteScalar(string command, DbParameter[] parameters)
        {
            var connection = m_connection;

            try
            {
                using (TDbCommand cmd = CreateCommand(command, connection))
                {
                    cmd.CommandType = CommandType.Text;
                    if (parameters != null && parameters.Length > 0)
                    {
                        cmd.Parameters.AddRange(CreateParameters(parameters));
                    }

                    await connection.OpenAsync();
                    await cmd.ExecuteNonQueryAsync();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }
    }
}
