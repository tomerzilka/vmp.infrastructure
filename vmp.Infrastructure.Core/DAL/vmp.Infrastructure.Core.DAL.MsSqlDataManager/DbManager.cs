﻿using System;
using System.Data.Common;
using System.Data.SqlClient;
using vmp.Infrastructure.Core.DataElements.Abstracts;

namespace vmp.Infrastructure.Core.DAL.MsSqlDataManager
{
    public class DbManager : BaseDbManager<SqlConnection, SqlCommand, SqlParameter>
    {
        protected override SqlParameter CreateParameter(DbParameter value)
        {
            return new SqlParameter
            {
                ParameterName = value.ParameterName,
                Value = value.Value == null ? DBNull.Value : value.Value
            };
        }
    }
}
