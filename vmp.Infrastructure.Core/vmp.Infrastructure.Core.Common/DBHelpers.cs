﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vmp.Infrastructure.Core.Common
{
    public class DBHelpers
    {
        public Nullable<T> ValueOrNull<T>(IDataReader reader, int ordinal) where T : struct
        {
            object value = reader.GetValue(ordinal);
            if (value == DBNull.Value) return null;
            else return (T)value;
        }
    }
}
