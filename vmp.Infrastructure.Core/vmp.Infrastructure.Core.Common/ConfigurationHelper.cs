﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vmp.Infrastructure.Core.Common
{
    public class ConfigurationHelper
    {
        public object GetConfigurationSectionValue(string sectionName)
        {
            return ConfigurationManager.GetSection(sectionName);
        }

        public T GetConfigurationSectionValue<T>(string sectionName)
        {
            return (T)ConfigurationManager.GetSection(sectionName);
        }

        public IDictionary<string, string> GetConfigurationSection(string sectionName)
        {
            Dictionary<string, string> retDic = new Dictionary<string, string>();
            var sectionProperties = ConfigurationManager.GetSection(sectionName) as NameValueCollection;
            
            if (sectionProperties.Count > 0)
            {
                foreach (var key in sectionProperties.AllKeys)
                {
                    retDic.Add(key, sectionProperties[key]);
                }
            }

            return retDic;
        }

        public IDictionary<string, string> GetConfigurationSection(params string[] xPath)
        {
            string formattedXPath = string.Join("/", xPath);
            Dictionary<string, string> retDic = new Dictionary<string, string>();
            var section = ConfigurationManager.GetSection(formattedXPath);
            var sectionProperties = section as NameValueCollection;
            if (sectionProperties.Count > 0)
            {
                foreach (var key in sectionProperties.AllKeys)
                {
                    retDic.Add(key, sectionProperties[key]);
                }
            }

            return retDic;
        }

        public string GetAppSettingsParameter(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public T GetAppSettingsParameter<T>(string key)
        {
            return (T)Convert.ChangeType(ConfigurationManager.AppSettings[key], typeof(T));
        }
    }
}
