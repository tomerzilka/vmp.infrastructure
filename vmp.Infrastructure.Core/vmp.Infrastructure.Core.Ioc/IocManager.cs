﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Linq.Expressions;
using vmp.Infrastructure.Core.DataElements.Interfaces;

namespace vmp.Infrastructure.Core.Ioc
{
    public class IocManager : IIocManager
    {
        private string m_componentsPath;
        private CompositionContainer m_container;

        [ImportMany(typeof(IIocControl), AllowRecomposition = true)]
        private IEnumerable<IIocControl> m_importedControls;

        private IEnumerable<IGrouping<Type, IIocControl>> m_groupedControls;

        public IocManager()
        {
            m_componentsPath = AppContext.BaseDirectory;

            LoadAssemblies();
        }

        private void LoadAssemblies()
        {
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new DirectoryCatalog(m_componentsPath));
            m_container = new CompositionContainer(catalog);
            m_container.ComposeParts(this);
            m_groupedControls = m_importedControls.GroupBy(x => x.GetType().GetInterfaces().Last(y => y.Name != nameof(IIocControl)));
        }

        public T FirstOrDefault<T>()
        {
            if (m_groupedControls == null)
            {
                return default(T);
            }

            Type requestedType = typeof(T);
            IEnumerable<T> filteredByType = m_groupedControls.First(x => x.Key == requestedType).Cast<T>();
            T retObj = filteredByType.FirstOrDefault();

            return retObj;
        }

        public T FirstOrDefault<T>(Expression<Func<T, bool>> predicate)
        {
            if (m_groupedControls == null)
            {
                return default(T);
            }

            Type requestedType = typeof(T);
            IEnumerable<T> filteredByType = m_groupedControls.SingleOrDefault(x => x.Key == requestedType).Cast<T>();
            if (filteredByType == null)
            {
                return default(T);
            }

            T retObj = filteredByType.FirstOrDefault(predicate.Compile());

            return retObj;
        }

        public TType FirstOrDefault<TType, TGroup>(Expression<Func<TType, bool>> predicate)
        {
            if (m_groupedControls == null)
            {
                return default(TType);
            }

            Type requestedGroup = typeof(TGroup);

            IEnumerable<TGroup> filteredByGroup = m_groupedControls.Single(x => x.Key == requestedGroup).Cast<TGroup>();
            if (filteredByGroup == null)
            {
                return default(TType);
            }

            TType retObj = Enumerable.OfType<TType>(filteredByGroup).FirstOrDefault(predicate.Compile());

            return retObj;
        }

        public IEnumerable<T> Many<T>()
        {
            if (m_groupedControls == null)
            {
                return null;
            }

            Type requestedType = typeof(T);
            var filteredByType = m_groupedControls.Where(x => x.Key == requestedType);

            return filteredByType.SelectMany(x => x).Cast<T>();
        }

        public T Instansiate<T>(Expression<Func<T, bool>> predicate) where T : Instansiatable<T>
        {
            var staticInstance = FirstOrDefault<T>(predicate);
            if (staticInstance == null)
            {
                return default(T);
            }

            return staticInstance.CreateInstance();
        }
    }
}
