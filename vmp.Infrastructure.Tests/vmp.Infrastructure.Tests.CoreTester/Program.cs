﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vmp.Infrastructure.Core.Common.Loggers;
using vmp.Infrastructure.Core.DataElements.Interfaces;

namespace vmp.Infrastructure.Tests.CoreTester
{
    class Program
    {
        static void Main(string[] args)
        {
            ILog logger = new Log4NetLogger("TestLogger");
            logger.Info("test");
        }
    }
}
